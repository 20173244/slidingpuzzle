import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SlidePuzzleMain {

    static GameMap gameMap;
    static JFrame frame;

    public static void main(String[] args) {
        startGame();
    }

    public static void startGame() {
        frame = new JFrame("SlidingPuzzle");
        frame.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (GameMap.hasWon() || GameMap.hasLost()) {
                    startGame();
                }
            }
        });
        gameMap = new GameMap(3);
        frame.setLayout(new GridLayout(4, 3));
        gameMap.addButtons(frame);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(681, 908);
        frame.setResizable(false);
        frame.setVisible(true);
    }
}

