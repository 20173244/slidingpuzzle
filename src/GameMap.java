import javax.imageio.ImageIO;
import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;

public class GameMap {
    private static Button[][] gameMap;
    private static int size;
    private static int emptyIndex;
    public static boolean hasWon;
    public static int moves;
    private static JLabel movesLeftLabel;
    private static JLabel emptySpot;
    private static JLabel fullImage;

    public GameMap(int size) {
        this.gameMap = new Button[size][size];
        this.size = size;
        fillGameMap();
    }

    private void fillGameMap() {
        try {
            moves = 100;
            movesLeftLabel = new JLabel();
            fullImage = new JLabel(new ImageIcon(ImageIO.read(getClass().getResource("resources/vilnius-full-small-image.png"))));
            emptySpot = new JLabel(new ImageIcon(ImageIO.read(getClass().getResource("resources/vilnius-empty-spot.png"))));
            hasWon = false;
            ArrayList<Integer> list = new ArrayList<>();
            for (int i = 0; i < size * size - 1; i++) {
                list.add(i);
            }
            Collections.shuffle(list);
            for (int i = 0; i < size * size - 1; i++) {
                gameMap[list.get(i) / size][list.get(i) % size] = new Button(new ImageIcon(getClass().getResource("resources/vilnius-" + (i) + ".png")), list.get(i) / size, list.get(i) % size, i);
            }
            emptyIndex = size * size - 1;
            gameMap[size - 1][size - 1] = new Button(new ImageIcon(getClass().getResource("resources/vilnius-empty-piece.png")), size - 1, size - 1, size * size - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean move(int x, int y) {
        //moveLeft
        if (y != 0 && gameMap[x][y - 1].getIndex() == emptyIndex) {
            swap(x, y - 1, x, y);
            return true;
        }
        //moveRight
        if (y != size - 1 && gameMap[x][y + 1].getIndex() == emptyIndex) {
            swap(x, y + 1, x, y);
            return true;
        }
        //moveUp
        if (x != 0 && gameMap[x - 1][y].getIndex() == emptyIndex) {
            swap(x - 1, y, x, y);
            return true;
        }
        //moveDown
        if (x != size - 1 && gameMap[x + 1][y].getIndex() == emptyIndex) {
            swap(x + 1, y, x, y);
            return true;
        }
        return false;
    }

    public static boolean hasWon() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (gameMap[i][j].getIndex() != i * size + j) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean hasLost() {
        if (moves <= 0) {
            return true;
        }
        return false;
    }

    public static void addButtons(JFrame frame) {
        frame.add(emptySpot);
        frame.add(movesLeftLabel);
        frame.add(fullImage);
        movesLeftLabel.setText(" ".repeat(20) + moves + " moves left");
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                frame.add(gameMap[i][j]);
            }
        }
    }

    private static void swap(int x2, int y2, int x1, int y1) {
        moves--;

        Button temp = gameMap[x1][y1];
        gameMap[x1][y1] = gameMap[x2][y2];
        gameMap[x2][y2] = temp;

        int tempCoordinateX = gameMap[x1][y1].getCoordinateX();
        gameMap[x1][y1].setCoordinateX(gameMap[x2][y2].getCoordinateX());
        gameMap[x2][y2].setCoordinateX(tempCoordinateX);

        int tempCoordinateY = gameMap[x1][y1].getCoordinateY();
        gameMap[x1][y1].setCoordinateY(gameMap[x2][y2].getCoordinateY());
        gameMap[x2][y2].setCoordinateY(tempCoordinateY);


        SlidePuzzleMain.frame.revalidate();
    }

    public static void removeButtons() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                gameMap[i][j].setVisible(false);
            }
        }
        movesLeftLabel.setVisible(false);
        fullImage.setVisible(false);
        emptySpot.setVisible(false);
    }

}
