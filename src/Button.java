import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


class Button extends JButton {

    private int coordinateX;
    private int coordinateY;
    private int index;

    public Button(ImageIcon image, int x, int y, int index) {
        super(image);
        this.coordinateX = x;
        this.coordinateY = y;
        this.index = index;
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                GameMap.move(coordinateX, coordinateY);
                GameMap.hasWon = true;
                if (GameMap.hasWon()) {
                    SlidePuzzleMain.frame.setContentPane(new JLabel(new ImageIcon(getClass().getResource("resources/Vilnius-You-Won.png"))));
                    GameMap.removeButtons();
                } else if (GameMap.hasLost()) {
                    SlidePuzzleMain.frame.setContentPane(new JLabel(new ImageIcon(getClass().getResource("resources/Vilnius-You-Lost.png"))));
                    GameMap.removeButtons();
                } else {
                    GameMap.addButtons(SlidePuzzleMain.frame);
                }
                SlidePuzzleMain.frame.revalidate();
                SlidePuzzleMain.frame.repaint();
            }
        });
    }

    public int getIndex() {
        return index;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

}
